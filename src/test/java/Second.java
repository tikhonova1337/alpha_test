import io.qameta.allure.junit4.DisplayName;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Second {

    protected static WebDriver driver;

    //Метод таймаута
    private  void threadSleep(int milliseconds){
        try {
            Thread.sleep( milliseconds);
        } catch (Exception ignored) {

        }
    }

    @Before
    public void beforeTest() {
        driver.get("https://yandex.ru/");
    }
    @BeforeClass
    public static void setup() {


        try{
            Properties props = new Properties();
            props.load(new FileInputStream("config.ini"));
            System.setProperty("webdriver.chrome.driver", props.getProperty("CHROME_DRIVER"));
        } catch(Exception e) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        }

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.setExperimentalOption("useAutomationExtension", false);
        options.addArguments("disable-infobars");
        options.addArguments("--disable-extensions");
        options.addArguments("--start-maximized");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-proxy");

        driver = new ChromeDriver(options);
        driver.manage().window().setSize(new Dimension(1366, 768));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        driver.get("https://yandex.ru/");

    }

    @Test
    @DisplayName("Тест мобильные телефоны Samsung")
    public void samsung() {

        //Переходим в раздел Маркета на странице https://yandex.ru/
        driver.findElement(By.cssSelector("a[data-id='market']")).click();

        //Открываем меню
       driver.findElement(By.className("n-w-tab__control-hamburger")).click();

       //Переходим в раздел Мобильные телефоны
        driver.findElement(By.linkText("Мобильные телефоны")).click();

        //Выставляем фильтр по производителю Samsung
        driver.findElement(By.cssSelector("label[for='7893318_153061']")).click();

        //Ждем 5 секунд пока применяется фильтр
        threadSleep(5000);

        //Выставляем фильтр по цене от 40000
        driver.findElement(By.id("glpricefrom")).sendKeys("40000");

        //Ждем 5 секунд пока применяется фильтр
        threadSleep(5000);

        //Заносим наименование первого телефона из списка в переменную namePhone
        WebElement phoneElement = driver.findElement(By.className("n-snippet-cell2__title"));
        String phoneNameList = phoneElement.getText();

        //Кликаем на первый телефон в списке
        phoneElement.findElement(By.tagName("a")).click();

        //Ждем 5 секунд пока страница с выбранным товаром откроется
        threadSleep(5000);

        //Заносим наименование телефона в переменную titlePhone
        WebElement titleElement = driver.findElement(By.cssSelector("h1[class='title title_size_28 title_bold_yes']"));
        String phoneNameDetail = titleElement.getText();


        //Сравниваем две переменные namePhone и titlePhone с названием телефона
        Assert.assertEquals(phoneNameList, phoneNameDetail);

    }



    @Test
    @DisplayName("Тест наушники марки Beats")
    public void beats(){
        //Переходим в раздел Маркета на странице https://yandex.ru/
        driver.findElement(By.cssSelector("a[data-id='market']")).click();

        //Открываем меню
        driver.findElement(By.className("n-w-tab__control-hamburger")).click();

        //Переходим в раздел Наушники и Bluetooth-гарнитуры
        driver.findElement(By.linkText("Наушники и Bluetooth-гарнитуры")).click();

        //Выставляем фильтр по производителю Beats
        driver.findElement(By.cssSelector("label[for='7893318_8455647']")).click();

        //Ждем пока применяется  фильтр
        threadSleep(5000);

        //Выставляем фильтр по цене от 17000 до 25000
        driver.findElement(By.id("glpricefrom")).sendKeys("17000");
        driver.findElement(By.id("glpriceto")).sendKeys("25000");

        //Ждем пока применяется  фильтр
        threadSleep(5000);

        //Заносим наименование первых наушников из списка в переменную nameHeadphones
        WebElement headphonesListElement = driver.findElement(By.className("n-snippet-cell2__title"));
        String headphonesList = headphonesListElement.getText();


        //Кликаем на первые наушники  в списке
        headphonesListElement.findElement(By.tagName("a")).click();

        //Ждем пока страница с выбранным товаром загрузится
        threadSleep(5000);

        //Заносим наименование наушников в переменную titleHeadphones
        WebElement headphonesElement = driver.findElement(By.cssSelector("h1[class='title title_size_28 title_bold_yes']"));
        String headphonesDetail = headphonesElement.getText();

        //Сравниваем две переменные headphones и titleHeadphones с названием наушников
        Assert.assertEquals(headphonesList, headphonesDetail);

    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}

