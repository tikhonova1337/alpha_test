import io.qameta.allure.junit4.DisplayName;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Third {

    protected static WebDriver driver;
    private static String searchUrl = "https://www.google.ru/";

    //Метод для прокрутки страницы
    private void scrollToBottom() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
    }

    //Метод таймаута
    private  void threadSleep(int milliseconds){
        try {
            Thread.sleep( milliseconds);
        } catch (Exception ignored) {

        }
    }

    @BeforeClass
    public static void setup() {

        try {
            Properties props = new Properties();
            props.load(new FileInputStream("config.ini"));
            System.setProperty("webdriver.chrome.driver", props.getProperty("CHROME_DRIVER"));
        } catch (Exception e) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        }

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox");
        options.setExperimentalOption("useAutomationExtension", false);
        options.addArguments("disable-infobars");
        options.addArguments("--disable-extensions");
        options.addArguments("--start-maximized");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-proxy");

        driver = new ChromeDriver(options);
        driver.manage().window().setSize(new Dimension(1366, 768));
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        driver.get(searchUrl);

    }

    @Test
    @DisplayName("Открытие сайта Альфа-Банка, копирование текста из абзаца <О нас> на сайте job.alfabank.ru ")
    public void alfaJobs() {
        //Метод который в наименование созданного файла пишет дату и время запуска теста
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

        //В поисковой google строке вводим Альфа Банк
        driver.findElement(By.cssSelector("input[class='gLFyf gsfi']")).sendKeys("Альфа-Банк");

        //Нажимаем на кнопку показать результаты
        driver.findElement(By.cssSelector("input[class='gNO89b']")).click();

        //Из результатов поиска кликаем на странуцу в которой есть фраза "Альфа-Банк - кредитные и дебетовые карты, кредиты наличными"
        driver.findElement(By.partialLinkText("Альфа-Банк - кредитные и дебетовые карты, кредиты наличными")).click();

        //В массиве вкладок из двух раскрытых вкладок выбираем сайт Альфа - Банка
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

        //Скроллим вниз страницы
        scrollToBottom();

        //Ждем 2 секунды
        threadSleep(2000);

        //Клик на вкладку вакансии
        driver.findElement(By.linkText("Вакансии")).click();

        // На странице с вакансиями скроллим на середину страницы, и берем текст из заголовка "О нас" и заносим его в 2 переменные
        threadSleep(2000);
        scrollToBottom();

       //Берем текст из первого абзаца
        String aboutAs = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/h3")).getText();

        //Берем текст из второго абзаца
        String titleSecond = driver.findElement(By.xpath("/html/body/div[2]/div[2]/div/div  ")).getText();

        //Берем названия браузера
        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
        String browserName = cap.getBrowserName().toLowerCase();

        //Заносим полученный текст из двух переменных в файл txt
        PrintWriter out = null;
        try {

            out = new PrintWriter(timeStamp + "_" + browserName + "_" + URI.create(searchUrl).getAuthority() + ".txt");
            out.println(aboutAs);
            out.println(titleSecond);
            out.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }


}
