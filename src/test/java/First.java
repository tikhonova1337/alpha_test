import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

public class First {

    public static void main(String[] args) {
        try (FileReader reader = new FileReader("src/n.txt")) {
            char[] buf = new char[256];
            int c;
            while ((c = reader.read(buf)) > 0) {

                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                String[] ary = new String(buf).split(",");
                Integer[] intArr = new Integer[ary.length];

                for (int i = 0; i < ary.length; i++) {
                    intArr[i] = Integer.parseInt(ary[i].replace(" ", ""));
                }
                Arrays.sort(intArr);
                System.out.println(Arrays.toString(intArr));

                Arrays.sort(intArr, Collections.reverseOrder());
                System.out.println(Arrays.toString(intArr));


            }
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

}
